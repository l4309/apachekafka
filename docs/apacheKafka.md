# Apache Kafka Documentation

## What is Kafka?
> **Apache Kafka** is a fast, reliable, scalable and [fault-tolerent](https://en.wikipedia.org/wiki/Fault_tolerance) ___messaging system___ which enable communication between ___producers___ and ___consumers___ using ___message based topic___.

## What is Messaging System?
> The main task of messaging system is to transfer the data from one application to another, so that the applications can mailnly work on data without worrying about sharing it.

> Distributed messaging is based on the reliable message queuing process. Messages are queued not-synchronously between the messaging system and client applications.

There are two types of messaging patterns:
1. Point-to-Point Messaging System
2. Publish-Subscribe Messaging System

- **Point-to-Point Messaging System**
In this messaging system, messages continue to remain in a queue. More than one consumer can consume the messages in the queue, _but only one consumer can consume a particular message_.

	After the consumer reads the message in the queue, the message disappears from the  queue.

- **Publish-Subscribe Messaging System**
In this messaging system, messages continue to remain in a _Topic_. Contrary to _Point-to-Point messaging system_, consumers can take more then one topic and consume every message in that topic. Message producers are known as _Publishers_ and Kafak consumers are known as _Subscribers_.

## History of Apache-Kafka
Previously, LinkedIn was facing the issue of low latency ingestion of huge amount of data from the website into a lambda architecture which could be able to process real-time events.

As a solution, Apache Kafka was developed in the year 2010, since none of the solutions was available to deal with this drawback, before.

However, there were technologies available for batch processing, but the deployment details of those technologies were shared with the downstream users. Hence, while it comes to Real-time Processing, those technologies were not enough suitable. Then, in the year 2011 Kafka was made public.

## Need of Apache Kafka Cluster

As we all know, there is an enormous volume of data used in Big Data. For data, there are two main challenges. One is how to collect and manage a large volume of data and the other one is the analysis of the collected data. For dealing with such challenges, you need to have a messaging system.